const express = require('express');
const cors = require('cors');
const {nanoid} = require('nanoid');
const app = express();

require('express-ws')(app);

const port = 8000;

app.use(cors());

const activeConnections = {}

app.ws('/canvasApp', (ws, req) => {
    const id = nanoid();
    activeConnections[id] = ws;

    ws.on('message', msg => {
        const decoded = JSON.parse(msg);
        console.log(decoded);
        switch (decoded.type) {
            case 'NEW_DRAW':
                Object.keys(activeConnections).forEach(key => {
                    const connection = activeConnections[key];
                    connection.send(JSON.stringify({
                        type: 'CREATE_DRAW',
                        draw: decoded.draw,
                    }));
                });
                break;
            default:
                console.log('Unknown type: ', decoded.type);
        }
    })

    ws.on('close', () => {
        console.log('Client disconnected!');
        delete activeConnections[id];
    });
});

app.listen(port, () => {
    console.log('Server works on port: ', port);
});