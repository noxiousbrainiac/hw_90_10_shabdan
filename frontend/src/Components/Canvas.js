import React, {useEffect, useRef, useState} from 'react';

const Canvas = () => {
    const [state, setState] = useState({
        mouseDown: false,
        pixelsArray: []
    });

    const canvas = useRef(null);
    const ws = useRef(null);

    useEffect(() => {
        ws.current = new WebSocket('ws://localhost:8000/canvasApp');

        ws.current.onmessage = (event) => {
            const decoded = JSON.parse(event.data);
            const {draw} = decoded;
            if (decoded.type === "CREATE_DRAW") {
                setState(prevState => ({
                    ...prevState,
                    pixelsArray: draw
                }));

                decoded.draw.forEach(item => {
                    imageRender({
                        clientX: item.x,
                        clientY: item.y,
                    });
                });
            }
        }
    }, []);

    const imageRender = event => {
        const clientX = event.clientX;
        const clientY = event.clientY;
        setState(prevState => {
            return {
                ...prevState,
                pixelsArray: [...prevState.pixelsArray, {
                    x: clientX,
                    y: clientY
                }]
            };
        });

        const context = canvas.current.getContext('2d');
        const imageData = context.createImageData(1, 1);
        const d = imageData.data;

        d[0] = 0;
        d[1] = 0;
        d[2] = 0;
        d[3] = 255;

        context.putImageData(imageData, event.clientX, event.clientY);
    };

    const canvasMouseMoveHandler = event => {
        if (state.mouseDown) {
            return imageRender(event);
        }
    };

    const mouseDownHandler = event => {
        setState({...state, mouseDown: true});
    };


    const mouseUpHandler = event => {
        setState({...state, mouseDown: false, pixelsArray: []});
        ws.current.send(JSON.stringify({
            type: "NEW_DRAW",
            draw: state.pixelsArray,
        }));
    };

    return (
        <>
            <canvas
                ref={canvas}
                style={{border: '1px solid black'}}
                width={800}
                height={600}
                onMouseDown={mouseDownHandler}
                onMouseUp={mouseUpHandler}
                onMouseMove={canvasMouseMoveHandler}
            />
        </>
    );
};

export default Canvas;